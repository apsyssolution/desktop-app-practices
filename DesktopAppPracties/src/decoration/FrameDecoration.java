package decoration;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Dialog.ModalExclusionType;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FrameDecoration extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameDecoration frame = new FrameDecoration();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameDecoration() {
		setAlwaysOnTop(true);
		setUndecorated(true);
		setTitle("Sigin Up");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 827, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 255, 255));
		panel.setBounds(10, 11, 807, 458);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(SystemColor.desktop);
		panel_1.setBounds(0, 0, 256, 458);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\LUCIFER\\Downloads\\Administrator-icon.png"));
		lblNewLabel.setBounds(63, 65, 156, 132);
		panel_1.add(lblNewLabel);
		
		JLabel lblUserLogin = new JLabel("User Login ");
		lblUserLogin.setForeground(SystemColor.textHighlightText);
		lblUserLogin.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblUserLogin.setBounds(79, 220, 121, 39);
		panel_1.add(lblUserLogin);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(SystemColor.activeCaptionBorder);
		panel_2.setBounds(255, 0, 552, 458);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		JPanel panel_3 = new JPanel();
		panel_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});
		panel_3.setBackground(SystemColor.inactiveCaptionText);
		panel_3.setBounds(515, 11, 27, 25);
		panel_2.add(panel_3);
		
		JLabel lblX = new JLabel("X");
		lblX.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel_3.add(lblX);
		
		textField = new JTextField();
		textField.setBounds(185, 128, 230, 33);
		panel_2.add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(185, 230, 230, 33);
		panel_2.add(passwordField);
		
		JLabel lblUserName = new JLabel("User Name:");
		lblUserName.setIcon(new ImageIcon("C:\\Users\\LUCIFER\\Desktop\\Work\\Inventory\\Icons\\an.png"));
		lblUserName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblUserName.setBounds(185, 92, 106, 25);
		panel_2.add(lblUserName);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setIcon(new ImageIcon("C:\\Users\\LUCIFER\\Desktop\\Work\\Inventory\\Icons\\User-Interface-Password-icon (2).png"));
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPassword.setBounds(185, 197, 106, 25);
		panel_2.add(lblPassword);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.setForeground(SystemColor.window);
		btnLogin.setBackground(SystemColor.inactiveCaptionText);
		btnLogin.setIcon(new ImageIcon("C:\\Users\\LUCIFER\\Desktop\\Work\\Inventory\\Icons\\User-Interface-Login-icon.png"));
		btnLogin.setBounds(261, 294, 106, 33);
		panel_2.add(btnLogin);
	}
}
